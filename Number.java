public enum Number
{
    ace(11),
    two(2),
    three(3),
    four(4),
    five(5),
    six(6),
    seven(7),
    eight(8),
    nine(9),
    ten(10),
    jack(10)
    {
        public String toString()
        {
            return "jack";
        }
    },
    queen(10)
    {
        public String toString()
        {
            return "queen";
        }
    },
    king(10)
    {
        public String toString()
        {
            return "king";
        }
    };
    //many to string methods for jack queen and king

    //creating the value variable
    private final int value;

    //constructor for the Enum
    Number(int value)
    {
        this.value = value;
    }

    //toString method
    public String toString()
    {
        if (value == 11)
        {
            return "ace";
        }
        else
        {
            return Integer.toString(value);
        }
    }

    //get value method to get the values within the enums
    public int getValue(){
        return value;
    }
}
