public class Card 
{
    private Symbol symbol;
    private Number number;

    //constructor
    public Card(Symbol sym, Number num)
    {
        this.symbol = sym;
        this.number = num;
    }

    //gets the value of the card
    public int getValue()
    {
        return number.getValue();
    }

    //toString method
    public String toString()
    {
        return this.number + " of " + this.symbol;
    }
}
