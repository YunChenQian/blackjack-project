public class DynamicArray
{
    private Card[] values;
    private int next;

    //constructor
    public DynamicArray()
    {
        this.values = new Card[100];
        this.next = 0;
    }

    // add -- takes as input string and adds it to the next available space in the array
    public void add(Card card)
    {
        this.values[this.next] = card;
        this.next++;
    }

    //removes the last card in the array
    public void remove()
    {
        this.values[this.next - 1] = null;
        this.next--;
    }

    //returns the first card
    public Card firstCard()
    {
        return this.values[this.next - 1];
    }

    //returns the amount of cards in the array
    public int numOfCards()
    {
        return this.next;
    }

    //changePosition -- changes the position of that index to the last
    public void changePosition(int position)
    {
        Card placeHolder = this.values[position];
        for (int i = position; i < this.next; i++)
        {
            this.values[i] = this.values[i + 1];
        }
        this.values[next - 1] = placeHolder;
    }

    //calculate total value and does the ace calulation
    public int calculateValue()
    {
        boolean hasEleven = false;
        int total = 0;
        for (int i = 0; i < this.next; i++)
        {
            int value = this.values[i].getValue();
            if(value == 11)
            {
                hasEleven = true;
            }
            total += value;
        }
        if(hasEleven && total > 21)
        {
            total -= 10;
        }
        return total;
    }

    //toString -- for testing purposes
    public String toString()
    {
        String returnString = "";
        for (int i = 0; i < this.next; i++)
        {
            returnString += this.values[i] + ", ";
        }
        return returnString;
    }
}


    