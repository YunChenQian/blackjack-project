import java.util.Random;
public class Deck
{

    Random rand = new Random();

    private DynamicArray deck = new DynamicArray();
    private Card card;
    private int amount = 0;

    //deck constructor
    public Deck()
    {
        for (Symbol symbol : Symbol.values())
        {
            for (Number number: Number.values())
            {
                card = new Card(symbol, number);
                deck.add(card);
                amount++;
            }
        }
    }


    //length -- returns the number of elements in the array
    public int length()
    {
        return amount;
    }


    //shuffle -- shuffles the array
    public void shuffle()
    {
        int timesShuffled = rand.nextInt(50, 200);
        for (int i = 0; i < timesShuffled; i++)
        {
            int random = rand.nextInt(52);
            this.deck.changePosition(random);
        }
    }

    //method to pull a card from index from array
    public Card pullCard()
    {
        Card pulled = deck.firstCard();
        deck.remove();
        return pulled;
    }

    //toString -- for testing purposes
    public String toString()
    {
        return "cards: " + this.deck;
    }
}
