import java.util.*;
public class BlackJack
//problem in asking for player to keep playing
{
    public static void main(String[]args)
    {
        System.out.println("Welcome to blackjack! \n");
        boolean play = true;
        int amountOfPlayers = scanForPlayer();
        Player[] group = new Player[amountOfPlayers];
        initiatePlayers(amountOfPlayers, group);
        addNamesToAll(group.length, group);
        howMuchDidAllBring(group);
        do
        {
            Deck deck = new Deck();
            deck.shuffle();
            Dealer dealer = new Dealer();
            allPlayersBet(group);
            playStartingTurnDealer(dealer, deck);
            for (int i = 0; i < group.length; i++)
            {
                boolean stillGoing = true;
                if (!playerOut(group[i]))
                {
                    playStartingTurnPlayer(group[i], deck);
                    while (stillGoing)
                    {
                        stillGoing = keepPlaying(group[i], deck);
                    }
                }
            }
            dealerPlays(dealer, group, deck);
            play = askAllPlayOrLeave(group);
            resetAllPlayerDeck(group);
            play = restartRound(play, group);
        }
        while(play);
        System.out.println("Thanks for playing! Please come again!");
    }    
    static Scanner scan = new Scanner(System.in);

    //initiates the players
    private static void initiatePlayers(int length, Player[] players)
    {
        for (int i = 0; i < length; i++)
        {
            players[i] = new Player();
        }
    }

    //scans and checks how many players there is 
    private static int scanForPlayer()
    {
        int amountOfPlayers = 0;
        boolean works = false;
        while(!works)
        {
            try
            {
                System.out.println("How many players is there?");
                amountOfPlayers = scan.nextInt();
                if (amountOfPlayers > 0)
                {
                    works = true;
                }
                else
                {
                    System.out.println("Please enter a positive number");
                }
            }
            catch(InputMismatchException error)
            {
                System.out.println("Please enter an integer");
                scan.nextLine();
            }
        }
        scan.nextLine();
        return amountOfPlayers;
    }

    //get a name for player
    private static String name(int number)
    {
        number++;
        System.out.println("Enter your name, Player" + number);
        return scan.nextLine();
    }

    //adds name to each player
    private static void addNamesToAll(int length, Player[] group)
    {
        for (int i = 0; i < length; i++)
        {
            group[i].addName(name(i));
        }
    }

    //ask for the player to bet and verifies
    private static double bet(Player player)
    {
        boolean works = false;
        double bet = 0;
        while(!works)
        {
            try
            {
                if (!(player.howMuchDoIHave() <= 0))
                {
                    System.out.println("Enter the amount that you would like to bet " + player.returnName() + ". You currently have: " + player.howMuchDoIHave() + "$");
                    bet = scan.nextDouble();
                    if(bet > 0 && bet <= player.howMuchDoIHave())
                    {
                        works = true;
                    }
                }
                else
                {
                    System.out.println(player.returnName() + ", you sadly do not have enough money to bet");
                    works = true;
                }
            }
            catch (InputMismatchException error)
            {
                System.out.println("Please enter a double");
                scan.nextLine();
            }
        }
        player.removeMoney(bet);
        scan.nextLine();
        return bet;
    }

    //checks if a player is out and removes them
    private static boolean playerOut(Player players)
    {
        boolean isSomeoneOut = false;
        if (players.howMuchDoIHave() <= 0 && players.returnBet() == 0)
        {
            isSomeoneOut = true;
        }
        return isSomeoneOut;
    }

    //asks all players to bet
    private static void allPlayersBet(Player[] player)
    {
        for (int i = 0; i < player.length; i++)
        {
            if(!playerOut(player[i]))
            {
                player[i].setBet(bet(player[i]));
            }
        }
    }

    //pull card and add to dealer or player method (Overloaded)
    private static void pullACard(Player player, Deck currentDeck)
    {
        player.addToHand(currentDeck.pullCard());
    }
    private static void pullACard(Dealer dealer, Deck currentDeck)
    {
        dealer.add(currentDeck.pullCard());
    }

    //play starting turn player
    private static void playStartingTurnPlayer(Player player, Deck currentDeck)
    {
        pullACard(player, currentDeck);
        pullACard(player, currentDeck);
        System.out.println(player);
    }

    //play starting turn for Dealer
    private static Card playStartingTurnDealer(Dealer dealer, Deck currentDeck)
    {
        pullACard(dealer, currentDeck);
        System.out.println(dealer);
        return currentDeck.pullCard();
    }

    //plays until that player cannot play the turn anymore
    private static void playATurn(Player playing, Deck currentDeck)
    {
        boolean works = false;
        while(!works)
        {
            if(!(playing.handValue() == 21))
            {
                System.out.println("Would you like to Stand (s), Hit (h) or Double Down (d)?");
                String option = scan.next();
                System.out.println();
                if(option.equals("s"))
                {
                    System.out.println(playing);
                    works = true;
                }
                else if(option.equals("h"))
                {
                    pullACard(playing, currentDeck);
                    System.out.println(playing);
                    works = true;
                }
                else if(option.equals("d"))
                {
                    if(playing.howMuchDoIHave() >= playing.returnBet())
                    {
                        works = true;
                        pullACard(playing, currentDeck);
                        playing.removeMoney(playing.returnBet());
                        playing.setBet(playing.returnBet()*2);
                        System.out.println(playing);
                        
                    }
                    else
                    {
                        System.out.println("You do not have enough money to double down");
                    }
                }
                else
                {
                    System.out.println("Please enter a correct input");
                }
                scan.nextLine();
            }
            else
            {
                works = true;
            }
        }
    }

    //keep a player playing until they cannot or stop
    private static boolean keepPlaying(Player player, Deck deck)
    {
        int startingHandValue = player.handValue();
        int startingNumOfCards = player.howManyCards();
        playATurn(player, deck);
        if(player.handValue() > 21)
        {
            System.out.println("You busted!!");
            player.setBet(0);
            return false;
        }
        else if(player.handValue() == 21)
        {
            return false;
        }
        else if(startingHandValue == player.handValue() && (startingNumOfCards == player.howManyCards()))
        {
            return false;
        }
        return true;
    }

    //checks if the players would like to play another turn
    private static boolean playAnotherRound(Player[] players)
    {
        int howManyPlaying = 0;  
        for (int i = 0; i < players.length; i++)
        {   
            boolean working = false;
            boolean isPlaying = false;
            while (!working)
            {
                if(!playerOut(players[i]))
                {
                    try
                    {
                        System.out.println("Would you like to keep playing? " + players[i].returnName() + " (y) for yes (n) for no.");
                        System.out.println();
                        String answer = scan.next();
                        if(answer.equals("y"))
                        {
                            isPlaying = true;
                            howManyPlaying++;
                        }
                        else if(answer.equals("n"))
                        {
                            isPlaying = false;
                        }
                    }
                    catch (InputMismatchException error)
                    {
                        System.out.println("Please enter a valid input");
                        scan.nextLine();
                    }
                }
                working = true;
                  
                
            }
            
            if (isPlaying == false)
            {
                System.out.println("Goodbye " + players[i].returnName() + "! Hope we will see you soon!");
                System.out.println("You have left with" + players[i].howMuchDoIHave() + "$");
                players[i].setBet(0);
                players[i].removeMoney(players[i].howMuchDoIHave());
            }
        }
        if(howManyPlaying > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //reset player bet
    private static void resetAllPlayerDeck(Player[] player)
    {
        for (int i = 0; i < player.length; i++)
        {
            player[i].resetDeck();
        }
    }

    //checks if all players busted
    private static boolean checkNotBusted(Player[] player)
    {
        for (int i = 0; i < player.length; i++)
        {
            if(player[i].handValue() <= 21)
            {
                return true;
            }
        }
        return false;
    }

    //checks if dealer has a bigger hand than everyone
    private static boolean checkIfDealerBigger(Dealer dealer, Player[] players)
    {
        boolean anyoneBigger = false;
        for (int i = 0; i < players.length; i++)
        {
            if(dealer.handValue() <= players[i].handValue() && players[i].handValue() <= 21)
            {
                anyoneBigger = true;
            }
            else
            {
                players[i].setBet(0);
            }
        }
        return anyoneBigger;
    }

    //group is out at this round
    private static boolean isGroupOutThisRound(Player[] players)
    {
        int howManyIn = 0;
        for(int i = 0; i < players.length; i++)
        {
            if(players[i].returnBet() > 0)
            {
                howManyIn++;
            }
        }
        if(howManyIn > 0)
        {
            return false;
        }
        return true;
    }

    //pays out the bets
    private static void payOutBets(Player[] player, Dealer dealer)
    {
        for (int i = 0; i < player.length; i++)
        {
            if(!playerOut(player[i]))
            {
                if(player[i].handValue() == 21 && !(dealer.handValue() == 21))
                {
                    player[i].addMoney(player[i].returnBet()*2.5);
                    System.out.println(player[i].returnName() +" YOU HAVE A BLACKJACK!!! Congradulations on winning " + player[i].returnBet()*2.5 + "$! Your current balance is: " +player[i].howMuchDoIHave());
                }
                else
                {
                    if(player[i].returnBet() == 0)
                    {
                        System.out.println(player[i].returnName() + ", you sadly did not win anything. Please try again!");
                    }
                    else
                    {
                        System.out.println(player[i].returnName() + ", you have won " + player[i].returnBet()*2 + "$");
                    }
                    player[i].addMoney(player[i].returnBet()*2);
                }
            }
        }
    }
    //dealer plays until they beat player
    private static void dealerPlays(Dealer dealer, Player[] players, Deck deck)
    {
        if(checkNotBusted(players))
        {
            if(checkIfDealerBigger(dealer, players))
            {
                while(!(dealer.handValue() >= 21) && !(dealer.handValue() == 17))
                {
                    //if(dealer.count)
                    for (int i = 0; i < players.length; i++)
                    {
                        if(dealer.handValue() > players[i].handValue() && dealer.handValue() <= 21)
                        {
                            players[i].setBet(0);
                        }
                        else if(dealer.handValue() == players[i].handValue() && dealer.handValue() < 21 && isGroupOutThisRound(players))
                        {
                            players[i].addMoney(players[i].returnBet());
                            players[i].setBet(0);
                        }
                    }
                    dealer.add(deck.pullCard());
                    System.out.println(dealer);
                    if(dealer.handValue() > 21)
                    {
                        System.out.println("Dealer busted! All players who didn't bust or lose wins!");
                    }
                }
            }
        }
        payOutBets(players, dealer);
    }

    //ask for how much each player brought
    private static void howMuchDidAllBring(Player[] player)
    {
        boolean works = false;
        double brought = 0;
        while(!works)
        {
            for(int i = 0; i < player.length; i++)
            {
                try
                {
                    System.out.println("Enter the amount that you brought " + player[i].returnName());
                    brought = scan.nextDouble();
                    if(brought > 0)
                    {
                        player[i].addMoney(brought);
                        works = true;
                    }
                    else
                    {
                        System.out.println("Please enter a positive number");
                    }
                }
                catch (InputMismatchException error)
                {
                    System.out.println("Please enter a double");
                    scan.nextLine();
                }
            }
            
        }
    }

    //if nobody can play ask if want to keep playing
    private static boolean restartRound(boolean canStillPlay, Player[] players)
    {
        boolean keepPlaying = false;
        if(canStillPlay == true)
        {
            return true;
        }
        else
        {
            boolean works = false;
            scan.nextLine();
            while(!works)
            {
                System.out.println("Would you like to restart another game? (y) for yes (n) for no");
                System.out.println();
                String answer = scan.nextLine();
                if(answer.equals("y"))
                {
                    works = true;
                    keepPlaying = true;
                    howMuchDidAllBring(players);
                }
                else if(answer.equals("n"))
                {
                    works = true;
                    keepPlaying = false;
                }
            }
        }
        return keepPlaying;
    }

    //asks everyone if they want to keep playing
    private static boolean askAllPlayOrLeave(Player[] players)
    {
        boolean isAtleastOne = false;
        boolean answer = playAnotherRound(players);
        if (answer == true)
        {
            isAtleastOne = true;
        }
        return isAtleastOne;
    }
}