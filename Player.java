import java.util.*;
public class Player 
{
    private DynamicArray hand;
    private double money;
    private String name;
    private boolean out;
    private double bet;
    
    Scanner scan = new Scanner(System.in);
    
    //player constructor
    public Player()
    {
        this.hand = new DynamicArray();
        this.money = 0;
        this.out = false;
    }

    //adds the user name
    public void addName(String nameInput)
    {
        this.name = nameInput;
    }
    
    //betAmount -- amount the player wants to bet
    public double betAmount()
    {
        boolean works = false;
        double thisBet = 0; 
        while (!works)
        {
            try
            {
                System.out.println("Enter how much you want to bet");
                thisBet = scan.nextDouble();
                if(thisBet > 0 && thisBet <= this.money)
                {
                    works = true;
                }
            }
            catch (InputMismatchException error1)
            {
                System.out.println("please enter a double number");
                scan.nextLine();
            }
        }  
        this.money -= thisBet;
        return thisBet;
    }

    //how much money the player would like to bet
    public void howMuchMoney()
    {
        boolean works = false;
        double bet = 0;
        do
        {
            try
            {
                System.out.println("How much money did you bring?");
                bet = scan.nextDouble();
                if (bet > 0)
                {
                    works = true;
                    this.money = bet;
                }
            }
            catch (InputMismatchException error2)
            {
                System.out.println("Please enter a double number");
                scan.nextLine();
            }
        }while (!works);
    }

    //returns the player's name
    public String returnName()
    {
        return this.name;
    }

    //add to the player's hand
    public void addToHand(Card card)
    {
        hand.add(card);
    }


    //add money to the player
    public void addMoney(double amount)
    {
        this.money += amount;
    }
    //remove money for player
    public void removeMoney(double amount)
    {
        this.money -= amount;
    }

    //return money
    public double howMuchDoIHave()
    {
        return this.money;
    }

    //return cards value
    public int handValue()
    {
        return hand.calculateValue();
    }

    //sets player to out
    public void setOutToTrue()
    {
        this.out = true;
    }

    //returns the value of out
    public boolean isOut()
    {
        return this.out;
    }

    //toString method
    public String toString()
    {
        return this.name + " has in their hands: " + this.hand + " VALUE: " + handValue() + "\n and has " + this.money + "$" ;
    }

    //sets the bet the player enter
    public void setBet(double bet)
    {
        this.bet = bet;
    }

    //return bet   
    public double returnBet()
    {
        return this.bet;
    }

    //reset hand
    public void resetDeck()
    {
        this.hand = new DynamicArray();
    }

    //returns how many cards the player has
    public int howManyCards()
    {
        return this.hand.numOfCards();
    }
}
