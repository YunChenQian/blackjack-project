public class Dealer 
{
    private DynamicArray hand;

    //dealer constructor
    public Dealer()
    {
        this.hand = new DynamicArray();
    }

    //adds a card to the dealers hand
    public void add(Card card)
    {
        hand.add(card);
    }

    //counts the value of cards in hand
    public int handValue()
    {
        return hand.calculateValue();
    }

    //to String method
    public String toString()
    {
        if(hand.numOfCards() == 1)
        {
            return "the Dealer has " + this.hand + "***Hidden Card*** VALUE: " + this.handValue();
        }
        else
        {
            return "the Dealer has " + this.hand + " VALUE: " + this.handValue();
        }
    }
}
